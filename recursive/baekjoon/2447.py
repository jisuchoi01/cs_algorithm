import sys

N = int(sys.stdin.readline())
stars = [[' ']*N for i in range(0, N)]

def print_stars():
    for i in range(0, len(stars)):
        for j in range(0, len(stars[0])):
            print(stars[i][j], end='')
        print()
        
def print_star(N=9, r=0, c=0):
    global stars
    if N == 3:
        for i in range(0, 3):
            for j in range(0, 3):
                if i == N//2 == j:
                    continue
                else:
                    stars[r+i][c+j] ='*'

        return

    for i in range(0, 3):
        for j in range(0, 3):
            s = r + i * N//3
            f = c + j * N//3
            if i == 1 == j:
                continue
            else:
                print_star(N//3, s, f)

print_star(N)
print_stars()
