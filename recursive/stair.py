# 계단 올라가기 문제 : 계단을 오를 때 1칸 또는 2칸만 올라갈 수 있다
# 총 계단수가 주어질 때 오를 수 있는 모든 경우의 수는?
N = 4
ans = 0
def solution(lv):
    global ans
    if lv is N:
        ans += 1
        return

    if lv < N:
        solution(lv + 1)
        
    if lv < N-1:
        solution(lv + 2)
    
solution(0)
print(ans)
