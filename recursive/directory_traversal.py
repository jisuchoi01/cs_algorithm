# 디렉토리 재귀 순회하여 파일명 출력
import os

def directory_travel(path):
    files = os.listdir(path)
    for f in files:
        file_abs = os.path.join(path, f)
        if os.path.isdir(file_abs):
            print('\033[92m' + f + '\033[0m')
            directory_travel(file_abs)
        else:
            print(f)

directory_travel('/home/dsparch/Project/code')
