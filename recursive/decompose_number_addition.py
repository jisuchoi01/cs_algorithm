# 자연수의 수 분할
# 아래 영상 참조
# https://www.youtube.com/watch?v=ZG3wPMorAAg&lc=z22bd34xcnybjn5js04t1aokgfqphiwkvsoqe4ucfyvcbk0h00410
# 자연수 n을 n이하의 자연수들로 분해하는 경우의 수 = \sum\limits^{N}_{k=1}{p(n, k)}
# 자연수 n이 n=n_1+n_2+n_3+ ... n_k로 표현되는 '1 <= k <= n' 과 n >= n_k를 만족하는 경우의 수
N = 10
def p(n, k):
    if k is 1:
        return 1
    if n < k:
        return 0

    case = 0
    for i in range(1, k+1):
        case += p(n-k, i)
    
    return case

ans = 0
for i in range(1, N+1):
    tmp = p(N, i)
    ans += tmp
    print(f'p({N}, {i})={tmp}')
print(f'ans={ans}')
