# 자연수의 수 분할
# 아래 영상 참조
# https://www.youtube.com/watch?v=ZG3wPMorAAg&lc=z22bd34xcnybjn5js04t1aokgfqphiwkvsoqe4ucfyvcbk0h00410
# 자연수 n을 n이하의 자연수들로 분해하는 경우의 수 = \sum\limits^{N}_{k=1}{p(n, k)}
# 자연수 n이 n=n_1+n_2+n_3+ ... n_k로 표현되는 '1 <= k <= n' 과 n >= n_k를 만족하는 경우의 수
N = 10
def p(n, k):
    if k is 1:
        return 1
    if n < k:
        return 0

    case = 0
    for i in range(1, k+1):
        case += p(n-k, i)
    
    return case


# 심화 : 제곱수로 분해하기
# n을 k개의 제곱수로 분해하기
def p_sqrt(n, k, limit):
    global N

    # 분할하려는 갯수보다 피분할 수가 작으면 자연수로 분할 할 수 없으므로
    if n < k or k < 0:
        #+print('END : n < k')
        return 0

    # n과 k가 같은 경우는 제곱수 1들의 합으로만 표현 가능한 경우
    # 또는 n < k는 아
    if n is k:
        #print('+1 END : n = k')
        return 1

    # n이 제곱수일 경우에만 자기자신 하나로 분해가능하다
    if k is 1:
        if n**0.5 == int(n**0.5):
            #print('+1 END : n은 제곱수')
            return 1
        else:
            #print('END : n은 제곱수 아님, k=1')
            return 0

    case = 0
    last_w = int(n**0.5)
    print(f'{limit}->{last_w}보다 작은 제곱수의 계수부터 구한다')

    # N보다 작은 제곱수의 계수부터 구해본다
    for i in range(0, last_w-1):
        next_w =(last_w-i)**2
        print(next_w)
        
        # 계수는 최대치부터 최소치까지 구한다.
        q, r = divmod(n, next_w)
        for j in range(0, q):
            next_n = n - next_w * (q-j)
            if k-q+j < 1:
                continue
            print(f'{next_w} 가 {(q-j)}개. 다음 p는 {next_n}를 {k-q+j} 개로 {next_w}보다 작은 수로 분할')
            case += p_sqrt(next_n, k-q-j, next_w)
            print(f'누적 경우의 수 : {case}')
    
    return case

ans = 0
for i in range(1, N+1):
    tmp = p(N, i)
    ans += tmp
    print(f'p({N}, {i})={tmp}')
print(f'ans={ans}')

# ans = 0
# for i in range(1, N+1):
#     tmp = p_sqrt(N, i, 9)
#     ans += tmp
#     print(f'p_sqrt({N}, {i})={tmp}')
# print(f'ans={ans}')
i = 6
print(f'p_sqrt({N}, {i})={p_sqrt(N, i, 9)}')
