import itertools

def get_column_to_unique_one(table, columns):
    ret = []
    for i in range(0, len(table)):
        unique_column = ''
        for c in columns:
            unique_column += table[i][c] + '/'
        ret.append(unique_column)
    return ret

def check_sub_element(combination, key_candidates):
    print(f'현재 후보키들 {key_candidates}')
    evaluation = False
    for subset in key_candidates:
        evaluation = set(combination) <= set(subset) or set(combination) >= set(subset)
        print(f'{set(combination)}가 {set(subset)} 중 있나? {evaluation}')
        if evaluation:
            return True
            
    return evaluation

def solution(table):
    answer = 0
    key_candidates = set()
    columns = [i for i in range(0, len(table[0]))]

    # 후보키 조합별로
    for i in range(1, len(table[0])+1):
        
        combinations = list(itertools.combinations(columns, i)) 
        for combination in combinations:
            unique_one = get_column_to_unique_one(table, combination)
            unique_set = set(unique_one)
            if len(unique_one) is len(unique_set):
                # 유일성 체크
                if check_sub_element(combination, key_candidates):
                    continue
                key_candidates.add(combination)
                answer += 1

    return answer
