def rotate2d(M):
    N = len(M)
    ret = []
    for k in range(0, N):
        ret.append([0 for i in range(0, N)])

    for i in range(0, N):        
        for j in range(0, N):
            ret[j][N-1-i] = M[i][j]
    return ret

def print2d(tdl, r=-1, c=-1):
    for i in range(0, len(tdl)):
        for j in range(0, len(tdl[0])):
            if i == r and j == c:
                print('\033[92m', end='')
            print(tdl[i][j], end=' ')
            print('\033[0m', end='')
        print()

    
def solution(key, lock):
    answer = False
    M = len(key)
    N = len(lock)
    padding = M - 1
    N += padding * 2

    # 좌우로 패딩시키기
    for i in range(0, padding):
        for record in lock:
            record.insert(0, 0) # 좌
            record.append(0) # 우

    # 상하 패딩
    for i in range(0, padding):
        record = [0 for i in range(0, N)]
        lock.insert(0, record)
        lock.append(record)
    
    for rotaion in range(0, 1):
        
        # 윈도우 슬라이딩 먼저
        for i in range(0, N - padding):
            for j in range(0, N - padding):
                key_correct = 0
                key_i = 0
                key_j = 0
                for r in range(max(padding, i), min(i+M, N-padding)):
                    for c in range(max(padding, j), min(j+M, N-padding)):
                        print(key[key_i][key_j], end=' ')
                    #     key_correct = lock[r][c] + key[key_i][key_j]
                        key_j += 1
                    key_i += 1
                    print()
                print2d(lock, i, j)
                print()
            
        key = rotate2d(key)
        
    return answer

print(solution([[1, 2, 3],
                [4, 5, 6],
                [7, 8, 9]], [[1, 1, 1], [1, 1, 0], [1, 0, 1]]))

