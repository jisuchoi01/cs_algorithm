def check_balance(parenthesis):
    balance = False
    # '(' 와 ')' 로만 이루어진 문자열이 있을 경우, '(' 의 개수와 ')' 의 개수가 같다면 이를 균형잡힌 괄호 문자열이라고 부릅니다.
    if parenthesis.count('(')  is parenthesis.count(')'):
        balance = True

    return balance

def check_correctness(parenthesis):
    correctness = False

    while '()' in parenthesis:
        parenthesis = parenthesis.replace('()', '')

    if len(parenthesis) is 0:
        correctness = True
    return correctness

def split_balance(parenthesis):
    _open = 0
    close = 0
    for i in range(len(parenthesis)):
        if parenthesis[i] == '(':
            _open += 1
        else:
            close += 1

        # 양쪽 등장 빈도가 같아야 함 같은 지점이 되는 구간 인덱스를 리턴
        if _open == close:
            return i+1
        
def generate_valid_one(p):
    # 1. 입력이 빈 문자열인 경우, 빈 문자열을 반환합니다. 
    if len(p) == 0:
        return ''

    # 2. 문자열 w를 두 "균형잡힌 괄호 문자열" u, v로 분리합니다.
    # 단, u는 "균형잡힌 괄호 문자열"로 더 이상 분리할 수 없어야 하며, v는 빈 문자열이 될 수 있습니다.
    i = split_balance(p)
    u, v = p[0:i], p[i:]

    # 3. 문자열 u가 "올바른 괄호 문자열" 이라면 문자열 v에 대해 1단계부터 다시 수행합니다. 
    if check_correctness(u):
        # 수행한 결과 문자열을 u에 이어 붙인 후 반환합니다. 
        return u + generate_valid_one(v)
    # 4. 문자열 u가 "올바른 괄호 문자열"이 아니라면 아래 과정을 수행합니다. 
    else:
        # 4-1. 빈 문자열에 첫 번째 문자로 '('를 붙입니다.
        # 4-2. 문자열 v에 대해 1단계부터 재귀적으로 수행한 결과 문자열을 이어 붙입니다.
        # 4-3. ')'를 다시 붙입니다. 
        empty = "(" + generate_valid_one(v) + ")"

        # 4-4. u의 첫 번째와 마지막 문자를 제거하고, 
        swap = u[1:-1]
        # 나머지 문자열의 괄호 방향을 뒤집어서 뒤에 붙입니다.
        swap = swap.replace('(', '_')
        swap = swap.replace(')', '(')
        swap = swap.replace('_', ')')
        
        # 4-5. 생성된 문자열을 반환합니다.
        return empty + swap
        

def solution(p):
    if check_correctness(p):
        return p
    return generate_valid_one(p)

print(solution('(()))('))
