def solution(food_times, k):
    answer = 0
    total_dish = len(food_times)
    for i in range(0, total_dish):
        print(food_times, k)
        if food_times[i] >= k:
            print(food_times[i], k)
            return -1
        k -= food_times[i]
        food_times[i] = 0
        if i >= total_dish:
            i = 0
    return answer

print(solution([5], 5), -1)
print(solution([1, 2, 3], 5), 3)
print(solution([3, 1, 2], 5), 1)
print(solution([3, 1, 2, 3], 5), 1)
