# Find rank using cumulative histogram

# below is input of algorithm. These mean 5 student A, B, C, D, E's test score
A = 70
B = 90
C = 80
D = 60
E = 95

# Make T array for score distribution. array 'H' means cumulative histogram
# We suggest score range is [0, 100]
T = [0 for i in range(0, 101)]
H = T.copy()

# Mark student's number on T distribution.
T[A] = 1
T[B] = 1
T[C] = 1
T[D] = 1
T[E] = 1

H[100] = T[100] 
for i in range(1, 101):
    H[100 - i] = H[100 - i + 1] + T[100 - i]

print(A, B, C, D, E)
print(H[A], H[B], H[C], H[D], H[E])
